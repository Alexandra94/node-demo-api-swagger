'use strict';

exports.addCity = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "country" : "Ukraine",
  "capital" : true,
  "name" : "Kyiv",
  "location" : "{}",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

exports.addOrUpdateCity = function(cityId,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

exports.deleteCity = function(cityId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

exports.searchCities = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "country" : "Ukraine",
  "capital" : true,
  "name" : "Kyiv",
  "location" : "{}",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
}, {
  "country" : "Ukraine",
  "capital" : true,
  "name" : "Kyiv",
  "location" : "{}",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

