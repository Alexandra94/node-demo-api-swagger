'use strict';

exports.addProduct = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "King Lear",
  "description" : "King Lear is a tragedy written by William Shakespeare.",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

exports.deleteProduct = function(productId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

exports.searchProduct = function(productId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "King Lear",
  "description" : "King Lear is a tragedy written by William Shakespeare.",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


exports.searchProducts = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "name" : "King Lear",
  "description" : "King Lear is a tragedy written by William Shakespeare.",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
}, {
  "name" : "King Lear",
  "description" : "King Lear is a tragedy written by William Shakespeare.",
  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

exports.searchReviews = function(productId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "product" : "d290f1ee-6c54-4b01-90e6-d701748f0852",
  "author" : "User1",
  "review" : "Great book",
  "dislikes" : 1.0,
  "likes" : 2.0
}, {
  "product" : "d290f1ee-6c54-4b01-90e6-d701748f0852",
  "author" : "User1",
  "review" : "Great book",
  "dislikes" : 1.0,
  "likes" : 2.0
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

