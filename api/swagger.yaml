---
swagger: "2.0"
info:
  description: "Products API"
  version: "1.0.0"
  title: "Simple Products API"
  license:
    name: "MIT"
host: "virtserver.swaggerhub.com"
basePath: "/Alexandra_Morozova/node-demo-api/1.0.0"
tags:
- name: "products"
  description: "Operations with products"
- name: "users"
  description: "Operations with users"
- name: "cities"
  description: "Operations with cities"
schemes:
- "https"
consumes:
- "application/x-www-form-urlencoded"
produces:
- "application/json"
paths:
  /products:
    get:
      tags:
      - "products"
      summary: "return all products"
      description: "By calling this endpoint you get all available products\n"
      operationId: "searchProducts"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "search results matching criteria"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Product"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Products"
    post:
      tags:
      - "products"
      summary: "Add a new product"
      description: "By calling this endpoint you add new product to the database\n"
      operationId: "addProduct"
      consumes:
      - "application/x-www-form-urlencoded"
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "New product"
        required: true
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Product"
      responses:
        200:
          description: "product successfully added"
          schema:
            $ref: "#/definitions/Product"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Products"
    x-swagger-router-controller: "product"
  /products/{productId}:
    get:
      tags:
      - "products"
      summary: "searches products"
      description: "By calling this endpoint you get particular product\n"
      operationId: "searchProduct"
      produces:
      - "application/json"
      parameters:
      - name: "productId"
        in: "path"
        description: "ID of product to return"
        required: true
        type: "string"
        format: "uuid"
      responses:
        200:
          description: "search results matching criteria"
          schema:
            $ref: "#/definitions/Product"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Product not found"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Products"
    delete:
      tags:
      - "products"
      summary: "Delete a product"
      operationId: "deleteProduct"
      parameters:
      - name: "productId"
        in: "path"
        description: "Product id to delete"
        required: true
        type: "string"
        format: "uuid"
      responses:
        200:
          description: "Product successfully deleted"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Product not found"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Products"
  /products/{productId}/reviews:
    get:
      tags:
      - "products"
      summary: "return reviews"
      description: "By calling this endpoint you get all reviews for particular product\n"
      operationId: "searchReviews"
      produces:
      - "application/json"
      parameters:
      - name: "productId"
        in: "path"
        description: "ID of product to return"
        required: true
        type: "string"
        format: "uuid"
      responses:
        200:
          description: "search results matching criteria"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Review"
        404:
          description: "Product not found"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Products"
  /users:
    get:
      tags:
      - "users"
      summary: "return all users"
      description: "By calling this endpoint you get all available users\n"
      operationId: "searchUsers"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "search results matching criteria"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/User"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Users"
    x-swagger-router-controller: "user"
  /users/{userId}:
    delete:
      tags:
      - "users"
      summary: "Delete a user"
      operationId: "deleteUser"
      parameters:
      - name: "userId"
        in: "path"
        description: "User id to delete"
        required: true
        type: "string"
        format: "uuid"
      responses:
        200:
          description: "User successfully deleted"
        400:
          description: "Invalid ID supplied"
        404:
          description: "User not found"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Users"
  /cities:
    get:
      tags:
      - "cities"
      summary: "return all ities"
      description: "By calling this endpoint you get all available cities\n"
      operationId: "searchCities"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "search results matching criteria"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/City"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Cities"
    post:
      tags:
      - "cities"
      summary: "Add a new city"
      description: "By calling this endpoint you add new city to the database\n"
      operationId: "addCity"
      consumes:
      - "application/x-www-form-urlencoded"
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "New city"
        required: true
        schema:
          type: "array"
          items:
            $ref: "#/definitions/City"
      responses:
        200:
          description: "city successfully added"
          schema:
            $ref: "#/definitions/City"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Cities"
    x-swagger-router-controller: "city"
  /cities/{cityId}:
    put:
      tags:
      - "cities"
      summary: "Updated city"
      description: "By calling this endpoint you add or update existing city\n"
      operationId: "addOrUpdateCity"
      produces:
      - "application/x-www-form-urlencoded"
      parameters:
      - name: "cityId"
        in: "path"
        description: "City id that need to be updated"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Updated city object"
        required: true
        schema:
          $ref: "#/definitions/City"
      responses:
        400:
          description: "Invalid city supplied"
        404:
          description: "User not found"
      x-swagger-router-controller: "Cities"
    delete:
      tags:
      - "cities"
      summary: "Delete a city"
      operationId: "deleteCity"
      parameters:
      - name: "cityId"
        in: "path"
        description: "City id to delete"
        required: true
        type: "string"
        format: "uuid"
      responses:
        200:
          description: "City successfully deleted"
        400:
          description: "Invalid ID supplied"
        404:
          description: "City not found"
        500:
          description: "Internal server error"
      x-swagger-router-controller: "Cities"
definitions:
  Product:
    type: "object"
    required:
    - "description"
    - "name"
    properties:
      id:
        type: "string"
        format: "uuid"
        example: "d290f1ee-6c54-4b01-90e6-d701748f0851"
      name:
        type: "string"
        example: "King Lear"
      description:
        type: "string"
        example: "King Lear is a tragedy written by William Shakespeare."
    example:
      name: "King Lear"
      description: "King Lear is a tragedy written by William Shakespeare."
      id: "d290f1ee-6c54-4b01-90e6-d701748f0851"
  User:
    type: "object"
    required:
    - "name"
    properties:
      id:
        type: "string"
        format: "uuid"
        example: "d290f1ee-6c54-4b01-90e6-d701748f0851"
      name:
        type: "string"
        example: "John"
      email:
        type: "string"
        example: "john@mail.com"
    example:
      name: "John"
      id: "d290f1ee-6c54-4b01-90e6-d701748f0851"
      email: "john@mail.com"
  City:
    type: "object"
    required:
    - "name"
    properties:
      id:
        type: "string"
        format: "uuid"
        example: "d290f1ee-6c54-4b01-90e6-d701748f0851"
      name:
        type: "string"
        example: "Kyiv"
      country:
        type: "string"
        example: "Ukraine"
      capital:
        type: "boolean"
        example: true
      location:
        type: "object"
        properties: {}
    example:
      country: "Ukraine"
      capital: true
      name: "Kyiv"
      location: "{}"
      id: "d290f1ee-6c54-4b01-90e6-d701748f0851"
  Review:
    type: "object"
    required:
    - "author"
    - "dislikes"
    - "likes"
    - "product"
    - "review"
    properties:
      author:
        type: "string"
        example: "User1"
      review:
        type: "string"
        example: "Great book"
      likes:
        type: "number"
        example: 2
      dislikes:
        type: "number"
        example: 1
      product:
        type: "string"
        format: "uuid"
        example: "d290f1ee-6c54-4b01-90e6-d701748f0852"
    example:
      product: "d290f1ee-6c54-4b01-90e6-d701748f0852"
      author: "User1"
      review: "Great book"
      dislikes: 1
      likes: 2
